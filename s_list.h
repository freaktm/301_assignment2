#ifndef SLIST_H_   /* Include guard */
#define SLIST_H_

//definitions
typedef struct scheduledItem S_ITEM;
typedef struct scheduledList S_LIST;

//A scheduled item
struct scheduledItem 
{
	char* name;
	uint32_t time;
	uint32_t repTime;
	S_ITEM* previous;
	S_ITEM* next;
};

//A scheduled List
struct scheduledList
{
	S_ITEM* head;
	S_ITEM* tail;
	uint32_t size;
	uint32_t time;
};

//method to create an item
S_ITEM* createScheduledItem(char* itemName, uint32_t time, uint32_t repTime);

//create an empty list
S_LIST* createScheduledList();

//insert an item, assumes no NULL items are passed
S_LIST* addItem(S_LIST* currentList, S_ITEM* newItem);

//output the schedled list of names with scheduled times -- for debugging
void outPutList(S_LIST* list);

//output list of items in a schedule with remaining time displayed
void outPutTimeRemaining(S_LIST* list);

S_LIST* clearList(S_LIST* list);

S_LIST* removeItem(S_LIST* list, S_ITEM* item);

S_LIST* removeAll(S_LIST* list, char* item);

S_LIST* updateTime(S_LIST* list, uint32_t newTime);

void executeTask(S_ITEM* task, uint32_t time);

S_ITEM* getItem(S_LIST* list, uint32_t n);

#endif
