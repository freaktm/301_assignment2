all: scheduler

scheduler: s_list.o scheduler.o
	gcc s_list.o scheduler.o -o scheduler

s_list.o: s_list.c
	gcc -g -Wall -c s_list.c

scheduler.o: scheduler.c
	gcc -g -Wall -c scheduler.c

clean:
	rm -rf *\.o schduler
