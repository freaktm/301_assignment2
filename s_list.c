#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include "s_list.h"


//method to create an item
S_ITEM* createScheduledItem(char* itemName, uint32_t time, uint32_t repTime)
{
	S_ITEM* i = (S_ITEM *) malloc(sizeof(S_ITEM));
	char *name = (char *) malloc((strlen(itemName)+1)*sizeof(char));
	strcpy(name, itemName);
	i->name = name;
	i->time = time;
	i->repTime = repTime;
	i->previous = NULL;
	i->next = NULL;
	return i;
}

//create an empty list
S_LIST* createScheduledList()
{
	S_LIST* l = (S_LIST *) malloc(sizeof(S_LIST));
	l->size = 0;
	l->time = 0;
	return l;
}



//insert an item, assumes no NULL items are passed
S_LIST* addItem(S_LIST* list, S_ITEM* new)
{
	S_ITEM* newItem = new;
	if (list->time <= newItem->time) //check to make sure a valid item was passed
	{
		//if the list is empty
		if(list->size == 0)
		{
			list->tail = newItem;
		}
		else if (list->size == 1)
		{
			if (list->tail->time > newItem->time)
			{
				list->head = newItem;
				list->tail->previous = newItem;
				newItem->next = list->tail;
			}
			else
			{
				list->head = list->tail;
				list->tail->next = newItem;
				list->tail = newItem;
				newItem->previous = list->head;
			}
		}
		else
		{
			if (list->tail->time <= newItem->time) // if new item is the latest scheduled item in list
			{			
				list->tail->next = newItem;
				newItem->previous = list->tail;
				list->tail = newItem;
			}
			else //iterate till position found
			{
				S_ITEM* currentItem = list->tail;
				while(currentItem->previous != NULL && currentItem->time > newItem->time)
				{
					currentItem = currentItem->previous;
				}

				//if new item is somewhere in the middle of list
				if (currentItem->previous != NULL)
				{
					newItem->next = currentItem->next;
					newItem->previous = currentItem;
					currentItem->next = newItem;
					newItem->next->previous = newItem;
				}
				else //if new item is now at head of list
				{
					newItem->next = list->head;
					newItem->next->previous = newItem;
					list->head = newItem;
				}

			}

		}

		list->size++; //increase the size of the list
			
	}

	//return the LIST
	return list;
}



//output the schedled list of names with scheduled times -- for debugging
void outPutList(S_LIST* list)
{
	printf("Upcoming Tasks\n");
	if(list != NULL)
	{
		S_ITEM* currentItem = NULL;
		if(list->size > 1)
		{
			currentItem = list->head;
		}
		else
		{	
			currentItem = list->tail;
		}
		while(currentItem != NULL)
		{
			printf(" %s %i\n",currentItem->name, currentItem->time);
			currentItem = currentItem->next;
		}

	}
}


//output list of items in a schedule with remaining time displayed
void outPutTimeRemaining(S_LIST* list)
{
	int time = list->time;
	printf("Upcoming tasks:\n");
	if(list != NULL)
	{
		S_ITEM* currentItem = NULL;
		if(list->size > 1)
		{
			currentItem = list->head;
		}
		else
		{	
			currentItem = list->tail;
		}
		while(currentItem != NULL)
		{
			printf("%s %i\n",currentItem->name, currentItem->time - time);
			currentItem = currentItem->next;
		}
	}
}


//clears all the items from a list
S_LIST* clearList(S_LIST* list)
{
	S_ITEM* currentItem = list->tail;

	while(currentItem != NULL)
	{
		S_ITEM* nextItem = currentItem->previous;
		free(currentItem->name);
		free(currentItem);
		currentItem = nextItem;
	}


	list->size = 0;
	list->head = NULL;
	list->tail = NULL;

	return list;	
}


//removes a specific item instance from the list, does not assume item is in list, so extra checking happens in method
S_LIST* removeItem(S_LIST* list, S_ITEM* item)
{
	S_LIST* newList = list;

	if(newList->size == 1 && item == newList->tail) // if removed item is only item in list
	{
		free(newList->tail);
		newList->tail = NULL;
		newList->size--;
	}
	else if (newList->size == 2) // if only two items are currently in list
	{
		if(newList->head == item) // if removed item was the head
		{
			newList->head = NULL;
			newList->tail->previous = NULL;
			newList->size--;
		}
		else if(newList->tail == item) // if removed item was the tail
		{
			newList->tail = newList->head;
			newList->head = NULL;
			newList->tail->next = NULL;
			newList->size--;
		}
	}
	else // if more than 2 items in list
	{
		S_ITEM* currentItem = newList->head;
		//iterate from head until item to be removed it found
		while(currentItem->next != NULL && currentItem != item)
		{
			currentItem = currentItem->next;
		}
		if(currentItem == item)
		{
			newList->size--;
			if(currentItem == newList->head) //if removed item is head
			{
				newList->head = currentItem->next;
				newList->head->previous = NULL;
			}
			else if(currentItem == newList->tail) // if removed item is tail
			{
				newList->tail = currentItem->previous;
				newList->tail->next = NULL;
			}
			else  // if removed item is somewhere in middle
			{
				currentItem->previous->next = currentItem->next;
				currentItem->next->previous = currentItem->previous;
			}

		}
		
	}	
	free(item->name);
	free(item);
	return newList;

}

// finds and returns an item, or null if not found
S_ITEM* getItem(S_LIST* list, uint32_t n)
{
	int i;
	S_ITEM* currentItem = list->tail;
	while(currentItem != NULL)
	{
		for(i = 0;i < list->size;i++)
		{
			if (i == n)
				return currentItem;
			currentItem = currentItem->previous;
		}
	}
	return currentItem = NULL;
}


//removes all items with the specified name from the list
S_LIST* removeAll(S_LIST* list, char* itemName)
{

	S_ITEM* currentItem = list->tail;
	if(list->size > 1)
		currentItem = list->head;

	
	while(currentItem != NULL)
	{
		
		S_ITEM* nextItem = currentItem->next;

		if(strcmp(currentItem->name, itemName) == 0)
		{
			list = removeItem(list, currentItem);
		}

		currentItem = nextItem;
	}
	return list;
}


S_LIST* updateTime(S_LIST* list, uint32_t newTime)
{
	if (list->size > 0)
	{
		//temp structs
		S_ITEM* currentItem = list->tail;

		//update time
		list->time = newTime;

		//get starting item for loop
		if(list->size > 1)
		{
			currentItem = list->head;
		}

		//iterate thru list until no more items to be processed
		while(currentItem != NULL)
		{
			S_ITEM* nextItem;
			if(currentItem->time <= newTime)
			{
				nextItem = currentItem->next;
				//execute task
				executeTask(currentItem, newTime);

				//if item is non repeating
				if(currentItem->repTime == 0)
				{		
					list = removeItem(list, currentItem);	
				}
				else //if item is repeating item
				{
					uint32_t newTime = currentItem->repTime + currentItem->time;
					S_ITEM* newItem = createScheduledItem(currentItem->name, newTime, currentItem->repTime);
					list = removeItem(list, currentItem);
					list = addItem(list, newItem);
				}
			}
			else
			{			
				nextItem = NULL;
			}

			currentItem = nextItem;
		}
	}

	//return updated schedule
	return list;
}


//method called to execute a task
void executeTask(S_ITEM* task, uint32_t time)
{
	printf("%i %s\n", task->time, task->name);
}

