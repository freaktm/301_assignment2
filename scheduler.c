#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include "s_list.h"


int main(int argc, char *argv[])
{
	//if not enough parameters on command line
 	if(argc < 2)
	{
 	   	printf("Usage:\n\t%s filename\n",argv[0]);
  	  	return -1;
 	}

	//pointer for a FILE
	FILE *file;
	
	//try open the file, if failed display error
	if((file = fopen(argv[1],"rt")) == NULL)
	{
 	   	printf("can't open %s\n",argv[1]);
 	   	return -2;
 	}

	//pointer to a scheduled list
 	S_LIST* schedule = createScheduledList();

	//buffer for data being read from file
    	char line [1024];

	//while the next line of data is not NULL
    	while(fgets(line,sizeof line,file)!= NULL) 
	{
		//main program


		char *token = strtok (line, " "); // setup a pointer for a token and get the first one
	      	char **params = NULL; // setup a pointer to an array of parameters
	      	int n = 0; //setup a counter for the array size

	     	//split the string on spaces and add each token to params array
	      	while(token)
		{
		  	params = realloc (params, sizeof(char*)* ++n);
		  	params[n-1] = token;
		  	token = strtok(NULL, " ");
		}

	      	
		//if the command was a ADDREP command
		if(strncmp("ADDREP", params[0], 6) == 0)
		{
			int len = strlen(params[3]) -1;
			char itemName[257] = "";
			strncpy(itemName, params[3], len);
			itemName[len] = '\0';
			S_ITEM* tmp = createScheduledItem(itemName, atoi(params[1]), atoi(params[2]));
			schedule = addItem(schedule, tmp);
		} 
		//if the command was a ADD command
		else if(strncmp("ADD", params[0], 3) == 0)
		{
			int len = strlen(params[2]) -1;
			char itemName[257] = "";
			strncpy(itemName, params[2], len);
			itemName[len] = '\0';
			S_ITEM* tmp = createScheduledItem(itemName, atoi(params[1]), 0);
			schedule = addItem(schedule, tmp);			
		} 
		//if the command was a TIME command
		else if(strncmp("TIME", params[0], 4) == 0)
		{
			schedule = updateTime(schedule, atoi(params[1]));
		}
		//if the command was a DEL command
		else if(strncmp("DEL", params[0], 3) == 0)
		{
			char itemName[257] = "";
			int len = strlen(params[1]) -1;
			strncpy(itemName, params[1], len);
			itemName[len] = '\0';
			schedule = removeAll(schedule, itemName);
		}
		//if the command was a LIST command
		else if(strncmp("LIST", params[0], 4) == 0)
		{
			outPutTimeRemaining(schedule);
		}
		//if the command was a CLEAR command
		else if(strncmp("CLEAR", params[0], 5) == 0)
		{
			schedule = clearList(schedule);
		}

	      	//free resources
	      	free(params);
    	}



	//free up resources
  	fclose(file); // close file
	//free up memory for list

	schedule = clearList(schedule);
	free(schedule); 


	return 0;


}


